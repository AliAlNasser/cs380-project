#version 330 core
layout (location = 1) in vec3 aPos;
layout (location = 0) in vec2 aTexture;


uniform float screen_ratio;
uniform vec2 screen_size;
uniform vec2 center;
uniform vec2 cj;
uniform float bound;
uniform vec2 st;

uniform float zoom;
uniform int iter;

uniform int fractal; 

out vec3 FragPos;
out vec2 texturePosition;

void main()
{
    gl_Position = vec4(aPos, 1.0f);
    FragPos = aPos;
    texturePosition = aTexture;
}
