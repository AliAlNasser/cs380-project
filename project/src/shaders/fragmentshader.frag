#version 410
out vec4 fragColor;

// in vec3 fragPosition;
in vec2 texturePosition;

uniform float screen_ratio;
uniform vec2 screen_size;
uniform vec2 center;
uniform vec2 julia_c;
uniform float bound;
uniform vec2 st;

uniform float zoom;
uniform int iter;

uniform int fractal; 
uniform int colormap;



float pi = 3.1415;


// I found this coloring function online :) 
vec4 map_to_color0(float t) {
    float b = 9.0 * (1.0 - t) * t * t * t;
    float g = 15.0 * (1.0 - t) * (1.0 - t) * t * t;
    float r = 8.5 * (1.0 - t) * (1.0 - t) * (1.0 - t)*t ;

    return vec4(r, g, b, 1.0);
}

vec4 map_to_color1(float t) {
    float g = 9.0 * (1.0 - t) * t * t * t;
    float b = 15.0 * (1.0 - t) * (1.0 - t) * t * t;
    float r = 8.5 * (1.0 - t) * (1.0 - t) * (1.0 - t)*t ;

    return vec4(r, g, b, 1.0);
}

vec4 map_to_color2(float t) {
    float g = 9.0 * (1.0 - t) * t * t * t;
    float r = 15.0 * (1.0 - t) * (1.0 - t) * t * t;
    float b = 8.5 * (1.0 - t) * (1.0 - t) * (1.0 - t)*t ;

    return vec4(r, g, b, 1.0);
}

vec4 map_to_color3(float t) {
    float r = 9.0 * (1.0 - t) * t * t * t;
    float g = 15.0 * (1.0 - t) * (1.0 - t) * t * t;
    float b = 8.5 * (1.0 - t) * (1.0 - t) * (1.0 - t)*t ;

    return vec4(r, g, b, 1.0);
}

vec4 map_to_color4(float t) {
    float b = 2/(1+exp(18*(t-0.25)*(t-0.25))); //9.0 * (1.0 - t) * t * t * t;
    float r = 2/(1+exp(18*(t-0.5)*(t-0.5)));
    float g = 2/(1+exp(18*(t-0.75)*(t-0.75)));// 8.5 * (1.0 - t) * (1.0 - t) * (1.0 - t)*t ;

    return vec4(r, g, b, 1.0);
}


vec4 map_to_color5(float t) {
    float r = 2/(1+exp(40*(t-0.25)*(t-0.25))); //9.0 * (1.0 - t) * t * t * t;
    float b = 2/(1+exp(40*(t-0.5)*(t-0.5)));
    float g = 2/(1+exp(40*(t-0.75)*(t-0.75)));// 8.5 * (1.0 - t) * (1.0 - t) * (1.0 - t)*t ;

    return vec4(r, g, b, 1.0);
}


void main()
{
    dvec2 z, c;

    c.x = screen_ratio * (gl_FragCoord.x / screen_size.x - 0.5);
    c.y = (gl_FragCoord.y / screen_size.y - 0.5);
    c.x /= zoom;
    c.y /= zoom;

    c.x += center.x;
    c.y += center.y;

    int i;
    if(fractal == 0){ // Julia

    
        z.x = 1.0 * texturePosition.x - 0.5;
        z.y = 1.0 * texturePosition.y - 0.5;

        z.x += center.x;
        z.y += center.y;

        z.x /= zoom;
        z.y /= zoom;
        
        

        for(i = 0; i < iter; i++) {
            double x = (z.x * z.x - z.y * z.y) + julia_c.x;
            double y =(z.y * z.x + z.x * z.y) + julia_c.y;

            if ((x * x + y * y) > bound) break;
            z.x = x;
            z.y = y;
        }
    }
    else if (fractal == 1){// Mandelbrot
        z = dvec2(0.0);
            for(i = 0; i < iter; i++) {
            double x = (z.x * z.x - z.y * z.y) + c.x;
            double y = (z.y * z.x + z.x * z.y) + c.y;
            
            if ((x * x + y * y) > bound) break;
            z.x = x;
            z.y = y;
        }    
    }

    // else { // visualizing roots of unity: z^5 -1
    //     z.x = 1.0 * texturePosition.x - 0.5;
    //     z.y = 1.0 * texturePosition.y - 0.5;

    //     z.x += center.x;
    //     z.y += center.y;

    //     z.x /= zoom;
    //     z.y /= zoom;
    //     bool br =false; 
        
    //     int Iter = 10*iter;
    //     for(i = 0; i < Iter; i++) {
    //         z = z - cmpxdiv(f5(z),f5der(z)); 

    //         for(int k =0; k<5; k++){    
    //             if (cmpxmag(z-roots(k,5))< 0.1){
    //               br = true;
    //               break;             
    //             } 
    //         }
    //         if(br){
    //             break;
    //         }
    //         z = z;
    //     }    

    // double t = double(i) / double(iter);
    // if(cmpxmag(z-roots(0,5))<.1){
    //     fragColor = map_to_color0(float(t));
    // }else if(cmpxmag(z-roots(1,5))<.1){
    //     fragColor = map_to_color1(float(t));
    // }else if(cmpxmag(z-roots(2,5))<.1){
    //     fragColor = map_to_color2(float(t));
    // }else if(cmpxmag(z-roots(3,5))<.1){
    //     fragColor = map_to_color3(float(t));
    // }
    // return;
    // }
    
    double t = double(i) / double(iter);


    if(colormap == 0){
        fragColor = map_to_color0(float(t));
    }else if(colormap == 1){
        fragColor = map_to_color1(float(t));
    }else if(colormap == 2){
        fragColor = map_to_color2(float(t));

     }else if(colormap == 3){
        fragColor = map_to_color3(float(t));
    }else if(colormap == 4){
        fragColor = map_to_color4(float(t));
    }else{
        fragColor = map_to_color5(float(t));
    }

}   

 
