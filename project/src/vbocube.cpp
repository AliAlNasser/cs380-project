#include "vbocube.h"

#define PI 3.141592653589793
#define TWOPI 6.2831853071795862
#define TWOPI_F 6.2831853f
#define TO_RADIANS(x) (x * 0.017453292519943295)
#define TO_DEGREES(x) (x * 57.29577951308232)


#include "glad/glad.h" 

// include glfw library: http://www.glfw.org/
#include <GLFW/glfw3.h>


#include <cstdio>

VBOCube::VBOCube()
{
    float vertices[] = {
        1.0f,  1.0f, 
        0.0f,  1.0f,
        1.0f,  1.0f, 
       -1.0f,  0.0f, 
        1.0f,  0.0f,
       -1.0f, -1.0f,
        0.0f,  0.0f,
        0.0f, -1.0f,  
        1.0f,  0.0f, 
        0.0f,  1.0f 
    };

    unsigned int indices[] = {  
        0, 1, 3,
        1, 2, 3
    };

    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), NULL);
    glEnableVertexAttribArray(6);
    // texture coord attribute
    glVertexAttribPointer(7, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(7);

}

void VBOCube::render() {
    glBindVertexArray(vaoHandle);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, ((GLubyte *)NULL + (0)));
}
