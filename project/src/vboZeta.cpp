#include "vboZeta.h"

#include "glad/glad.h" 

// include glfw library: http://www.glfw.org/
#include <GLFW/glfw3.h>


#include <cstdio>
#include <cmath>

#include <iostream>

VBOZeta::VBOZeta(int xrange, int zrange,int linspace) :
       xr(xrange), zr(zrange), dn((float) linspace) 
{
    xr = xrange/2*linspace; 
    zr = zrange/2*linspace; 
    nVerts = (2*xr+1)*(2*zr+1);
    elements = (2*xr+1)*(2*zr+1)*3;

    // Verts
    float * v = new float[3 * nVerts];
    // Normals
    float * n = new float[3 * nVerts];
    // Tex coords
    float * tex = new float[2 * nVerts];
    // Elements
    unsigned int * el = new unsigned int[elements];

    // Generate the vertex data
    generateVerts(v, n, tex, el);

    // Create and populate the buffer objects
    unsigned int handle[4];
    glGenBuffers(4, handle);

    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, (3 * nVerts) * sizeof(float), v, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, (3 * nVerts) * sizeof(float), n, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glBufferData(GL_ARRAY_BUFFER, (2 * nVerts) * sizeof(float), tex, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements * sizeof(unsigned int), el, GL_STATIC_DRAW);

    delete [] v;
    delete [] n;
    delete [] el;
    delete [] tex;

    // Create the VAO
    glGenVertexArrays( 1, &vaoHandle );
    glBindVertexArray(vaoHandle);

    glEnableVertexAttribArray(0);  // Vertex position
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer( (GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0 );

    glEnableVertexAttribArray(1);  // Vertex normal
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer( (GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0 );

    glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
    glEnableVertexAttribArray(2);  // Texture coords
    glVertexAttribPointer( (GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, 0 );

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);

    glBindVertexArray(0);
}

void VBOZeta::render() const {
    glBindVertexArray(vaoHandle);
    glDrawElements(GL_POINTS, elements, GL_UNSIGNED_INT, ((GLubyte *)NULL + (0)));
}

void VBOZeta::generateVerts(float * verts, float * norms, float * tex,
                             unsigned int * el)
{
	// Generate positions and normals
	// GLfloat theta, phi;
	// GLfloat thetaFac = (2.0 * PI) / slices;
	// GLfloat phiFac = PI / stacks;
	// GLfloat nx, ny, nz, s, t;
    
    // verts[ idx ] <- x 
    // verts[idx+1] <- y = f(x,z)
    // verts[idx+2] <- z 
    
    std::cout<<"range from "<<-xr/dn<<" to "<< xr/dn<<std::endl;
	GLuint idx = 0, tIdx = 0;
	for( int i = -xr; i <= xr; i++ ) {

		for( int j = -zr; j <= zr; j++ ) {
		    verts[idx  ] = (float) i/dn;
        
            verts[idx+2] = (float) (j/dn);

            verts[idx+1] = func(verts[idx],verts[idx+2]);
            // std::cout<<"i,j,k = "<<verts[idx]<<","<<verts[idx+1]<<","<<verts[idx+2]<<" , dn ="<<dn<<std::endl;
            norms[idx  ] = 2*verts[idx  ];
            norms[idx+1] = -1;
            norms[idx+2] = 2*verts[idx+2];;
            idx += 3;
            tex[tIdx] = (float) (verts[idx]+verts[idx+1]+verts[idx+2]);
            tex[tIdx+1] = (float) (verts[idx]+verts[idx+1]+verts[idx+2]);
            tIdx += 2;
		}
	}

	// Generate the element list
	// idx = 0;
	// for( int i = 0; i < slices; i++ ) {
	// 	GLuint stackStart = i * (stacks + 1);
	// 	GLuint nextStackStart = (i+1) * (stacks+1);
	// 	for( int j = 0; j < stacks; j++ ) {
	// 		if( j == 0 ) {
	// 			el[idx] = stackStart;
	// 			el[idx+1] = stackStart + 1;
	// 			el[idx+2] = nextStackStart + 1;
	// 			idx += 3;
	// 		} else if( j == stacks - 1) {
	// 			el[idx] = stackStart + j;
	// 			el[idx+1] = stackStart + j + 1;
	// 			el[idx+2] = nextStackStart + j;
	// 			idx += 3;
	// 		} else {
	// 			el[idx] = stackStart + j;
	// 			el[idx+1] = stackStart + j + 1;
	// 			el[idx+2] = nextStackStart + j + 1;
	// 			el[idx+3] = nextStackStart + j;
	// 			el[idx+4] = stackStart + j;
	// 			el[idx+5] = nextStackStart + j + 1;
	// 			idx += 6;
	// 		}
	// 	}
	// }

    for(int i =0; i<elements; i++ ){
        el[i] = i;
    }

    
}

int VBOZeta::getVertexArrayHandle() {
	return this->vaoHandle;
}


float VBOZeta::func(float x, float z){
    float r2 = exp(5*(x*x+z*z));
    // return exp(-(x*x+z*z));
    // if (r2 > 0.001){
    //      return 1/r2 -1;
    // }
    // else{
    //     return -1000;   
    // }
    return 1/(1+r2);
}
