// CS 380 - GPGPU Programming
// Programming Assignment #3

// system includes
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <math.h>
#include <sstream>
#include <algorithm> 
#include <array>

// library includes
// include glm: http://glm.g-truc.net/
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
// #define GLg_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform2.hpp>



#include "glad/glad.h" 
// include glfw library: http://www.glfw.org/
#include <GLFW/glfw3.h>

#include "glslprogram.h"


// for loading bmp files as textures
#include "bmpreader.h"




// pointer to the window that will be created by glfw
GLFWwindow* g_pWindow;

// window size
unsigned int g_uWindowWidth = 800;
unsigned int g_uWindowHeight = 600;

float zoom = 0.3f;
int fractal = 1, colormap=0; 
float cx = 0.0, cy = 0.0;
float cj_x = .323412, cj_y = 0.3941;
int iter = 200;

// timing
float deltaTime = 1.0f;
float lastFrame = 0.0f;


float bound = 2.0;
// a more complex mesh

// glsl program
GLSLProgram *g_glslProgram;
unsigned int VBO, VAO, EBO;

// glfw errors are reported to this callback
void errorCallback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}


void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{

	float mov = 3.0 * deltaTime * (1.0/zoom);
    float mov_julia = 0.1 * deltaTime;	

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS){
        cy = 0; cx = 0; zoom=0.3;
		cj_x = .323412, cj_y = 0.3941;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_RELEASE)
        cy += mov;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_RELEASE)
        cy -= mov;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_RELEASE)
        cx -= mov;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_RELEASE)
        cx += mov;
    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS){
        fractal = (fractal +1) %2;
    }
     if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS){
        colormap = (colormap +1) % 6;
    }

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
        cj_y += mov_julia;
        std::cout<<"(cx,cy) = ("<<cj_x<<", "<<cj_y<<")\n";
    }	
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
        cj_y -= mov_julia;
    	std::cout<<"(cx,cy) = ("<<cj_x<<", "<<cj_y<<")\n";
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
        cj_x += mov_julia;
   	 	std::cout<<"(cx,cy) = ("<<cj_x<<", "<<cj_y<<")\n";
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
        cj_x -= mov_julia;
    	std::cout<<"(cx,cy) = ("<<cj_x<<", "<<cj_y<<")\n";
    }
    if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS){
        cj_x = 0.5; cj_y = 0.5;
        std::cout<<"(cx,cy) = ("<<cj_x<<", "<<cj_y<<")\n";
    }
        

    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS) {
        iter += 1;
        if (iter < 0) { iter = 0; }
    }
    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS) {
        iter -= 1;
        std::cout<<"iter = "<<iter<<"\n";
    }

    if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS) {
        bound -= 0.1;
        if (bound < 0) { bound = 0; }
        std::cout<<"bound = "<<bound<<"\n";
    }
    if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS) {
        bound += 0.1;
    }
    
}


void cursorPosCallback(GLFWwindow* window, double x, double y){
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods){
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset){
	float z = 0.1 * zoom;
    if (yoffset > 0) {
        zoom += z;
    } else {
        zoom -= z;
    }

}




// query GPU functionality we need for OpenGL, return false when not available
bool queryGPUCapabilitiesOpenGL()
{
	// OPTIONAL: Check for required OpenGL functionality
	return true;
}



// check for opengl error and report if any
void checkForOpenGLError()
{
	GLenum error = glGetError();
	if (error==GL_NO_ERROR) {
		return;
	}
	std::string errorString = "unknown";
	std::string errorDescription = "undefined";

	if (error==GL_INVALID_ENUM)
	{
		errorString = "GL_INVALID_ENUM";
		errorDescription = "An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_VALUE)
	{
		errorString = "GL_INVALID_VALUE";
		errorDescription = "A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_OPERATION)
	{
		errorString = "GL_INVALID_OPERATION";
		errorDescription = "The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_INVALID_FRAMEBUFFER_OPERATION)
	{
		errorString = "GL_INVALID_FRAMEBUFFER_OPERATION";
		errorDescription = "The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag.";
	}
	else if (error==GL_OUT_OF_MEMORY)
	{
		errorString = "GL_OUT_OF_MEMORY";
		errorDescription = "There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.";
	}
	else if (error==GL_STACK_UNDERFLOW)
	{
		errorString = "GL_STACK_UNDERFLOW";
		errorDescription = "An attempt has been made to perform an operation that would cause an internal stack to underflow.";
	}
	else if (error==GL_STACK_OVERFLOW)
	{
		errorString = "GL_STACK_OVERFLOW";
		errorDescription = "An attempt has been made to perform an operation that would cause an internal stack to overflow.";
	}

	fprintf(stdout, "Error %s: %s\n", errorString.c_str(), errorDescription.c_str());
}

				  

// OpenGL error debugging callback
void APIENTRY glDebugOutput(GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar *message,
	const void *userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

	std::cout << "---------------" << std::endl;
	std::cout << "Debug message (" << id << "): " << message << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
	case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
	case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
	} std::cout << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
	case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
	case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
	case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
	} std::cout << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
	case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
	} std::cout << std::endl;
	std::cout << std::endl;
}



void loadShader()
{

	glEnable(GL_DEPTH_TEST);


	// Set up glsl program 
	g_glslProgram = new GLSLProgram();
	std::cout << "loading file vertexshader.vert" << std::endl;
	try {
		g_glslProgram->compileShader("./src/shaders/vertexshader.vert");
	} catch (GLSLProgramException e) {
		std::cout << "loading vertex shader failed." << std::endl;
		std::cout << e.what() << std::endl;

	}
	std::cout << "loading file fragmentshader.vert" << std::endl;
	try {
		g_glslProgram->compileShader( "./src/shaders/fragmentshader.frag" );
	} catch (GLSLProgramException e) {
		std::cout << "loading vertex shader failed." << std::endl;
		std::cout << e.what() << std::endl;
	}

	g_glslProgram->link();


} 


void printUsage(){ 
	fprintf(stdout, "================================ usage ================================\n");
	fprintf(stdout, "arrows: increment/decrement c\n");
	fprintf(stdout, "wasd: move around \n");
	fprintf(stdout, "scroll: zoom\n");
	fprintf(stdout, "0: reset\n");
	fprintf(stdout, "Q: reset c\n" );
	fprintf(stdout, "f: julia mandelbrot toggle.\n" );
	fprintf(stdout, "ESC: exit\n" );
	fprintf(stdout, "+/-: increase/decrease number of iterations\n" );
	fprintf(stdout, "b/v: increase/decrease limit\n" );
	
	
	
	fprintf(stdout, "=======================================================================\n");
}




// init application 
bool initApplication(int argc, char **argv)
{
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(glDebugOutput, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);


	std::string version((const char *)glGetString(GL_VERSION));
	std::stringstream stream(version);
	unsigned major, minor;
	char dot;

	stream >> major >> dot >> minor;

	assert(dot == '.');
	if (major > 3 || (major == 2 && minor >= 0)) {
		std::cout << "OpenGL Version " << major << "." << minor << std::endl;
	}
	else {
		std::cout << "The minimum required OpenGL version is not supported on this machine. Supported is only " << major << "." << minor << std::endl;
		return false;
	}

	// set callbacks
	glfwSetKeyCallback(g_pWindow, keyCallback);
	glfwSetCursorPosCallback(g_pWindow, cursorPosCallback);
	glfwSetMouseButtonCallback(g_pWindow, mouseButtonCallback);
	glfwSetScrollCallback(g_pWindow, scroll_callback);
	// default initialization
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST);

	// viewport
	glViewport(0, 0, g_uWindowWidth, g_uWindowHeight);



	return true;
}

// entry point
int main(int argc, char** argv)
{
	printUsage();
	// set glfw error callback
	glfwSetErrorCallback(errorCallback);

	// init glfw
	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	// init glfw window 
	
	g_pWindow = glfwCreateWindow(g_uWindowWidth, g_uWindowHeight, "CS380 - OpenGL Image Processing", nullptr, nullptr);
	if (!g_pWindow)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	

	// make context current (once is sufficient)
	glfwMakeContextCurrent(g_pWindow);

	// get the frame buffer size
	int width, height;
	glfwGetFramebufferSize(g_pWindow, &width, &height);

	// init the OpenGL API (we need to do this once before any calls to the OpenGL API)
	gladLoadGL();

	// query OpenGL capabilities
	if (!queryGPUCapabilitiesOpenGL())
	{
		// quit in case capabilities are insufficient
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	

	// init our application
	if (!initApplication(argc, argv)) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// setting up our scene
	loadShader();


	float vertices[] = {
        1.0f,  1.0f, 
        0.0f,  1.0f,
        1.0f,  1.0f, 
       -1.0f,  0.0f, 
        1.0f,  0.0f,
       -1.0f, -1.0f,
        0.0f,  0.0f,
        0.0f, -1.0f,  
        1.0f,  0.0f, 
        0.0f,  1.0f 
    };

    unsigned int indices[] = {  
        0, 1, 3,
        1, 2, 3
    };

    
   
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), NULL);
    glEnableVertexAttribArray(1);
    // texture coord attribute
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(0);


    // glutSwapBuffers();
    g_glslProgram->use();

    g_glslProgram->printActiveAttribs();
    g_glslProgram->printActiveUniforms();

    g_glslProgram->printActiveUniformBlocks();
	// start traversing the main loop
	// loop until the user closes the window 
	while (!glfwWindowShouldClose(g_pWindow))
	{


		float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		g_glslProgram->use();
		g_glslProgram->setUniform("screen_size", vec2((float) g_uWindowWidth, (float) g_uWindowHeight));
        g_glslProgram->setUniform("screen_ratio", (float) g_uWindowWidth / (float) g_uWindowHeight);
        g_glslProgram->setUniform("center", vec2(cx, cy));
        g_glslProgram->setUniform("julia_c", vec2(cj_x, cj_y));
        g_glslProgram->setUniform("zoom", zoom);
        g_glslProgram->setUniform("iter", iter);
        g_glslProgram->setUniform("bound", bound);
		g_glslProgram->setUniform("fractal", fractal);
		g_glslProgram->setUniform("colormap", colormap);

		// render one frame     
	    glBindVertexArray(VAO);
	    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	    		// swap front and back buffers 
		glfwSwapBuffers(g_pWindow);


		// poll and process input events (keyboard, mouse, window, ...)
		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
	glfwTerminate();
	return EXIT_SUCCESS;
}
